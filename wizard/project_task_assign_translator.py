# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.exceptions import Warning, UserError
import datetime
import logging

_logger = logging.getLogger(__name__)

class taskAssignTranslator(models.TransientModel):
	_name = 'wizard.task.assign_translator'
	
	available_from = fields.Datetime(string='Unavailable until', help="Date this translator is unavailable to.")
	chosen_translator = fields.Many2one(comodel_name='res.partner', string='Translator to Assign')
	warning_message = fields.Selection([
			('ineligible','This translator is marked as ineligible. This is due to them not having provided us with all the required paperwork to begin working for us. Please choose another translator to assign.'),
			('blacklist','This translator has been blacklisted and can not be assigned to this task. If they have been blacklisted in error you can change their status on their contact'),
			('poor','This translator has a history of poor quality translations or unprofessional behaviour. You can continue and assign them but you may want to consider another translator'),
			('unavailable','This translator is currently unavailable during the deadline you have set. Please select an available translator'),
			('limited','This translator is unavailable until a date close to the deadline set for this task. You may still assign this translator but may want to consider the time frame.'),
			('ok','There is no issue with assigning this translator')
		])
	is_valid = fields.Boolean(help="Signifies if this translator is allowed to be assigned to this task.")
	task = fields.Many2one(comodel_name='project.task')
	unavailable = fields.Boolean(default=False)
	
	@api.multi
	def assign_translator(self):
		try:
			data = {
				'x_translator': self.chosen_translator.id,
				'x_status': 'open',
			}
			self.task.write(data)
			if not self.task.x_purchase_order.exists():
				task_po = self.task._create_po(self.task)
				self.task.write({'x_purchase_order': task_po.id})
				action = {
					'type':'ir.actions.act_window',
					'res_model':'account.invoice',
					'views':[[False,'form']],
					'res_id':task_po.id,
					'target':'current',
					'flags':{'action_buttons':True},
				}
				return action
		except Exception:
			_logger.error("error assigning translator by wizard", exc_info=True)
			
	@api.multi
	@api.onchange('chosen_translator','chosen_translator.x_status','chosen_translator.x_eligible','chosen_translator.x_available_from')
	def check_new_translator(self):
		try:
			_logger.debug("entering onchange")
			translator = self.chosen_translator
			unavailable = False
			if not translator.x_eligible:
				warning_message = 'ineligible'
				is_valid = False
			else:
				if translator.x_status == 'blacklist':
					warning_message = 'blacklist'
					is_valid = False
				elif translator.x_status == 'poor':
					warning_message = 'poor'
					is_valid = True
				elif translator.x_status == 'unavailable':
					if self.task.date_deadline <= translator.x_available_from:
						warning_message = 'unavailable'
						is_valid = False
						unavailable = True
					elif self.task.date_deadline > translator.x_available_from:
						_logger.debug("availability is limited")
						warning_message = 'limited'
						is_valid = True
						unavailable = True
					self.available_from = translator.x_available_from
				else:
					warning_message = 'ok'
					is_valid = True
			
			self.warning_message = warning_message
			self.is_valid = is_valid
			self.unavailable = unavailable
		except Exception:
			_logger.error("error checking new translator", exc_info=True)
		# data = {
			# 'warning_message': warning_message,
			# 'is_valid': is_valid,
			# 'unavailable': unavailable,
		# }
		# self.write(data)
		
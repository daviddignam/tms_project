from openerp import SUPERUSER_ID
from openerp import models, fields, api
from openerp.osv import fields, osv
from openerp.exceptions import Warning, UserError
import logging

_logger = logging.getLogger(__name__)

class project_configuration(osv.osv_memory):
	_inherit = 'project.config.settings'
	
	_columns = {
		'default_po_tax': fields.many2one('account.tax', "Default Tax",
			help='Default Tax item used when automatically creating a purchase order'),
		'default_po_acc': fields.many2one('account.account', "Default PO Acc",
			help="Default Account used when creating Purchase Order(Normally your Acc Payable).", rel='default_po_acc_rel'),
		'default_po_line_acc': fields.many2one('account.account', "Default PO Line Acc",
			help="Default Account used when creating Lines on Purchase Order(Normally your Expenses Acc).", rel='default_line_acc_rel'),
		'default_po_journal': fields.many2one('account.journal', "Default PO Journal",
			help="Default Journal used to post PO Journal Entry(Normally your Supplier Bills Journal)."),
	}
	
	def set_default_default_po_tax(self, cr, uid, ids, context=None):
		try:
			config_value = self.browse(cr, uid, ids, context=context).default_po_tax
			check = self.pool['res.users'].has_group(cr, uid, 'base.group_system')
			self.pool.get('ir.values').set_default(cr, check and SUPERUSER_ID or uid, 'project.config.settings', 'default_po_tax', config_value.id)
		except Exception:
			_logger.error("Error setting value", exc_info=True)
			
	def set_default_default_po_acc(self, cr, uid, ids, context=None):
		try:
			config_value = self.browse(cr, uid, ids, context=context).default_po_acc
			check = self.pool['res.users'].has_group(cr, uid, 'base.group_system')
			self.pool.get('ir.values').set_default(cr, check and SUPERUSER_ID or uid, 'project.config.settings', 'default_po_acc', config_value.id)
		except Exception:
			_logger.error("Error setting value", exc_info=True)
	
	def set_default_default_po_line_acc(self, cr, uid, ids, context=None):
		try:
			config_value = self.browse(cr, uid, ids, context=context).default_po_line_acc
			check = self.pool['res.users'].has_group(cr, uid, 'base.group_system')
			self.pool.get('ir.values').set_default(cr, check and SUPERUSER_ID or uid, 'project.config.settings', 'default_po_line_acc', config_value.id)
		except Exception:
			_logger.error("Error setting value", exc_info=True)
			
	def set_default_default_po_journal(self, cr, uid, ids, context=None):
		try:
			config_value = self.browse(cr, uid, ids, context=context).default_po_journal
			check = self.pool['res.users'].has_group(cr, uid, 'base.group_system')
			self.pool.get('ir.values').set_default(cr, check and SUPERUSER_ID or uid, 'project.config.settings', 'default_po_journal', config_value.id)
		except Exception:
			_logger.error("Error setting value", exc_info=True)

